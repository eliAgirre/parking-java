package vista;

import gestor.GestorParking;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;;

/**
 * Extiende de la clase JFrame implementando MouseListener y ActionListener, y muestra un array bidimensional de JButton expresando plazas de color verde (deshabilitados) y de color rojo (habilitados). 
 * 
 * @author SP,EA
 * @see	VentanaDatosPlaza
 * @see GestorParking
 */

@SuppressWarnings("serial")
public class VentanaVer extends JFrame implements MouseListener,ActionListener{

	//Atributos de la clase
	private JPanel contentPane;
	private GestorParking parking; //Para poder usar los metodos de GestorParking
	private int filas=3; 
	private int columnas=20;
	private JButton[][] botones; //Array bidimensional de botones
	private JButton btnSalir; 
	private VentanaPrincipal frame1; //VentanaPrincipal para cargar los botones

	/**
     * Constructor sin par�metros y aparecen todos los componentes y crea la conexi�n con la base de datos.
     * 
     */
	public VentanaVer() {

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Vista del Parking");
		setIconImage(new ImageIcon(getClass().getResource("Img/logo.png")).getImage());
		setBounds(100, 100, 451, 302);
		
		componentes();
		
		parking=new GestorParking();
		
		frame1=new VentanaPrincipal(); //Instancia de la VentanaPrincipal

		botones=frame1.crearBotones(this); //Establece el array de los botones creados en la VentanaPrincipal
		
		this.verPlazas(); //M�todo que define los colores de cada plaza en la VentanaVer
		
	} //Cierre del constructor

	/**
     * Contiene todos los componentes para aparecer en la VentanaVer.
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label_0 = new JLabel("0");
		label_0.setBounds(21, 101, 21, 14);
		contentPane.add(label_0);
		
		JLabel label_1 = new JLabel("1");
		label_1.setBounds(21, 173, 21, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("2");
		label_2.setBounds(21, 241, 21, 14);
		contentPane.add(label_2);
		
		JLabel lblSeleccionaUnaPlaza = new JLabel("Selecciona una plaza");
		lblSeleccionaUnaPlaza.setBounds(21, 34, 146, 14);
		contentPane.add(lblSeleccionaUnaPlaza);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener((ActionListener) this);
		btnSalir.setBounds(599, 307, 89, 23);
		contentPane.add(btnSalir);
		
	} //Cierre del m�todo componentes
	
	/**
     * Establece los colores de los botones, habilitando los colores de fondo rojo para ver los datos de la plaza correspondiente y deshabilitando las plazas verdes que est�n libres. 
     *  
     * @return JButton[][] Devuelve un array bidimensional de botones definida por los colores y asociada al MouseListener y al ActionListener.
     *  
     */
	public JButton[][] verPlazas(){
		
		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){

				if(parking.libre(i,j)==false){ //Cuando la plaza este ocupada
					botones[i][j].setBackground(Color.RED);  //Establece el color del fondo del bot�n rojo
					botones[i][j].setEnabled(true); //Habilita el bot�n
				}
				else{ //Si la plaza esta libre
					botones[i][j].setEnabled(false);  //Deshabilita el bot�n
				}
				//eventos asociados a cada bot�n
				botones[i][j].addMouseListener((MouseListener) this);
				botones[i][j].addActionListener(new ActionListener() {

				   public void actionPerformed(ActionEvent event) {


				    	//Recorremos el array por cada piso
						for(int i=0;i<filas;i++){
							
							//Recorremos el array por cada plaza
							for(int j=0;j<columnas;j++){
								
								 if (event.getSource()==botones[i][j]){ //Evento asociado a cada bot�n del array bidimensional
									 try {
											 String codigo=botones[i][j].getText(); //Obtiene el c�digo del bot�n asociado
											 
											 
											 // Ejecuta la VentanaDatosPlaza.
											 VentanaDatosPlaza frame7; //Declara la VentanaDatosPlaza
											 frame7=new VentanaDatosPlaza(codigo); //Se crea la VentanaDatosPlaza y se le pasa como par�metro un String de c�digo para despu�s mostrar los datos dependiendo del c�digo de la plaza.
											 frame7.setVisible(true); //Hace visible la VentanaDatosPlaza
											 frame7.setLocationRelativeTo(null); //Localiza la la VentanaDatosPlaza dependiendo de la localizaci�n de la VentanaVer
											 setVisible(false); //Hace invisible la VentanaVer
											 
										} catch (SQLException e) {
											
											JOptionPane.showMessageDialog(null, e.getMessage()); //Aviso por pantalla de la excepci�n sql 
										}
					             }
							}
						}
			
				   }			
				 
			});
		
			}
		}
		
		return botones; //Devuelve el array bidimensional de botones
	}
    
	/**
	 * 
     * Responde al ActionListener al producir una acci�n.
     * 
     */
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource()==btnSalir){ //Al realizar el evento en el bot�n Salir cerrar� la VentanaVer.
			
			this.dispose(); //cierre de la VentanaVer
			
		}
	} //Cierre del m�todo action performed

	/**
	 * 
     * Responde al MouseListener cuando el rat�n hace un clic en el bot�n
     * 
     */
	public void mouseClicked(MouseEvent mouse) {} //Cierre del m�todo mouse clicked

	/**
	 * 
     * Responde al MouseListener cuando el rat�n pasa por encima del bot�n
     * 
     */
	public void mouseEntered(MouseEvent mouse) {

		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){
				
				if (mouse.getSource()==botones[i][j]){ //Al pasar por encima de un bot�n correspondido
	
					if(parking.libre(i,j)==true){
						botones[i][j].setBackground(Color.GREEN); //Establece el color del bot�n verde
						botones[i][j].setEnabled(false); //Deshabilita el bot�n
					}
					else{
						
						botones[i][j].setBackground(Color.BLUE); //Establece el color del fondo del bot�n azul
						botones[i][j].setForeground(Color.WHITE);  //Establece el color del texto del bot�n blanco
					}
	
				}
			}
		}
	} //Cierre del m�todo mouse entered

	/**
	 * 
     * Responde al MouseListener cuando el rat�n sale del bot�n
     * 
     */
	public void mouseExited(MouseEvent mouse) {
	
		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){
				
				if (mouse.getSource()==botones[i][j]){ //Al salir de un bot�n correspondido
					
					if(parking.libre(i,j)==false){ //Si la plaza esta ocupada
						botones[i][j].setBackground(Color.RED); //Establece el color del fondo del bot�n rojo
						botones[i][j].setForeground(Color.BLACK); //Establece el color del texto del bot�n negro
						botones[i][j].setEnabled(true); //Habilita el bot�n
					}
					else{ //Si la plaza esta libre
						botones[i][j].setEnabled(false); //Deshabilita el bot�n
					}
				}
			}
		}
	} //Cierre del m�todo mouse exited

	/**
	 * 
     * Responde al MouseListener cuando el rat�n presiona el bot�n
     * 
     */
	public void mousePressed(MouseEvent mouse) {
		
		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){
				
				if (mouse.getSource()==botones[i][j]){ //Al presionar de un bot�n correspondido
					botones[i][j].setForeground(Color.BLACK); //Establece el color del texto del bot�n negro
				}
			}
		}
	} //Cierre del m�todo mouse pressed

	/**
	 * 
     * Responde al MouseListener cuando el rat�n presiona y suelta el bot�n
     * 
     */
	public void mouseReleased(MouseEvent mouse) {} //Cierre del m�todo mouse released
    
} //Cierre de la clase 