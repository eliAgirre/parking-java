package vista;

import gestor.GestorParking;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;

import java.awt.*;

/**
 * Extiende de la clase JFrame implementando ActionListener y muestra un JTextField para que el usuario pueda introducir el c�digo de la plaza para salir del parking.
 * 
 * @author SP,EA
 * @see	VentanaPagar
 * @see GestorParking
 * @see VentanaPrincipal
 */

@SuppressWarnings("serial")
public class VentanaSalir extends JFrame implements ActionListener{
	
	//Atributos de la clase
	private JPanel contentPane;
	private GestorParking parking; //Para poder usar los metodos de GestorParking
	private JTextField textCodigo; //TextField para introducir el c�digo
	private JButton btnAceptar; //Bot�n para validar el c�digo introducido
	private String codigo; //Atributos de tipo String donde se guarda el c�digo


	/**
     * Constructor sin par�metros.
     * 
     */
	public VentanaSalir() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Salir");
		setIconImage(new ImageIcon(getClass().getResource("Img/salir.png")).getImage()); 
		setBounds(100, 100, 187, 165);
		
		componentes();
		
	} //Cierre del constructor

	/**
     * Contiene todos los componentes para aparecer en la VentanaSalir.
     *  
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInsertaElCodigo = new JLabel("Inserta el codigo:");
		lblInsertaElCodigo.setBounds(39, 28, 106, 14);
		contentPane.add(lblInsertaElCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setBounds(39, 54, 86, 20);
		contentPane.add(textCodigo);
		textCodigo.setColumns(10);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener((ActionListener) this);
		btnAceptar.setBounds(39, 93, 86, 23);
		contentPane.add(btnAceptar);
		
		parking=new GestorParking();

	} //Cierre del m�todo componentes
	
	/**

     * Validar el c�digo de tres n�meros para saber si est� dentro del rango entre 000-019,100-119 y 200-219.
     * @param codigo String que define los tres n�meros del c�digo de la plaza.
     * @return correcto Devuelve un true si el codigo introducido es v�lido entre el rango, si no coincide ninguna de los tres con las esas condiciones ser� false. 
     * 
     */
	private static boolean codigoValido(String codigo){
		
		boolean correcto=true;
		
		if(codigo.length()!=3){ //Si es distinto de 3 n�meros
			JOptionPane.showMessageDialog(null, "El codigo debe tener 3 n�meros.","ERROR", JOptionPane.WARNING_MESSAGE); //Aviso por pantalla 
			correcto=false;
		}
		
		for(int i=0;i<codigo.length();i++){
			
			String subString = codigo.substring(i, i+1);
			int subInt = Integer.parseInt(subString);
			
			if(i==0){ //El primer c�digo que define el piso debe estar entre el 0 y el 2.
				
				if(subInt<0 || subInt>2){
					
					correcto=false;
				}
			}
			if(i==1){ //El segundo caracter debe estar en 0 y 1 porque s�lo hay 20 plazas para cada piso.
				
				if(subInt<0 || subInt>1){
					correcto=false;
					
				}
			} 
			if(i==2){ //El �ltimo d�gito s�lo tendr� que ser entre el 0 y 9.
				
				if(subInt<0 || subInt>9){
					
					correcto=false;
					
				}
			}
		}
		
		return correcto;
	}

	
	/**
     * ActionPerformed responde al evento producido del ActionListener
     * 
     */
	public void actionPerformed(ActionEvent evento) { //la cabecera del un �nico actionPerformed
		
		if(evento.getSource()==btnAceptar){ //Al realizar el evento en el bot�n Aceptar, validar� el codigo pasando a la siguiente ventana (VentanaPagar).

			codigo=textCodigo.getText().toUpperCase(); //obtiene la matricula del JTextField.
		
			try {
				
				//Validar los campos textFields
				if(textCodigo.getText().equals("")){
					
					JOptionPane.showMessageDialog(textCodigo, "Introduce el codigo","ERROR", JOptionPane.WARNING_MESSAGE); //Aviso por pantalla de que el c�digo no se ha introducido
					JOptionPane.showMessageDialog(null, "Introduce 3 numeros"); //Aviso del formato del c�digo a introducir
				}
				
				else if(parking.plazaOcupada(codigo)==false&&codigoValido(codigo)){ //si la plaza est� ocupada
					
				
					VentanaPagar frame5; //Se declara la ventana VentanaPagar
					frame5 = new VentanaPagar(codigo); //Se crea el objeto ventana pasando el par�metro codigo
					frame5.setVisible(true); //La ventana ser� visible
					frame5.setLocationRelativeTo(null); //Localiza en el centro de la pantala dependiendo de la VentanaSalir.
					setVisible(false); //Hace que la VentanaSalir no sea visible
					
				}
				else{ //si la plaza est� libre
					
					JOptionPane.showMessageDialog(null, "No hay ning�n veh�culo en esta plaza","ERROR", JOptionPane.WARNING_MESSAGE);//muestra un aviso si el c�digo introducido no est� ocupado por un veh�culo.
					textCodigo.setText("");
				}
				
					
			} catch (SQLException e) {
				JOptionPane.showConfirmDialog(null, e.getMessage()); //un aviso del mensaje de la excepci�n sql
			}
			
		} //Cierre del evento asociado al bot�n Aceptar

	} //Cierre del m�todo actionPerformed
} //Cierre de la clase 