package vista;

import gestor.GestorBD;
import gestor.GestorParking;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;

import java.awt.*;

/**
 * Extiende de JFrame e implementa el ActionListener teniendo los atributos de JLabel, JTextField, JButton, otros atributos para hacer la conexi�n con la base de datos y un atributo de la clase GestorParking. 
 *  
 * @author SP,EA
 * @see GestorParking
 * @see GestorBD
 * @see	VentanaPrincipal
 */

@SuppressWarnings("serial")
public class VentanaPagar extends JFrame implements ActionListener{

	//Atributos de la clase
	private JPanel contentPane;
	private JTextField textMatricula; //La matricula del vehiculo obtenido por la bases de datos, que se muestra en JTextField
	private JTextField textMarca; //La marca del vehiculo obtenido por la bases de datos, que se muestra en JTextField
	private JTextField textModelo; //El modelo del vehiculo obtenido por la bases de datos, que se muestra en JTextField
	private JTextField textColor; //El color del vehiculo obtenido por la bases de datos, que se muestra en JTextField
	private JTextField textTiempo; //El tiempo transcurrido en la plaza correspondiente
	private JLabel lblCodigo;
	private JTextField textCodigo; //El c�digo din�mico que se muestra en el JTextField
	private JLabel lblCoste; 
	private JTextField textCoste; //JTextField para mostrar el coste dependiendo la hora transcurrida en la plaza correspondiente
	private String cadena; //Esta cadena de String es utilizada para las sentencias de sql
	private static ResultSet rs; //Devuelve los resultados
	private static Statement stmt; //Realiza la conexi�n con la base de datos
	private GestorParking parking; //Para poder usar los metodos de GestorParking
	private JButton btnAtras; //Bot�n para volver al anterior ventana
	private JButton btnPagar; //Bot�n para salir de la plaza
	

	/**
     * Constructor con un par�metro, aparecen todos los componentes y crea la conexi�n con la base de datos.
     * @param codigo String que define los tres n�meros del c�digo de la plaza.
	 * @throws SQLException 
     * 
     */
	public VentanaPagar(String codigo) throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Pagar la plaza");
		setIconImage(new ImageIcon(getClass().getResource("Img/pago.png")).getImage()); 
		setBounds(100, 100, 261, 278);

		componentes(); 
		
		parking=new GestorParking();
		
		GestorBD.conectar();
		stmt=GestorBD.conexion();
		
		cadena = "SELECT * FROM vehiculo WHERE codigo='"+codigo+"';"; //la sentencia sql se guarda en un String
		rs = GestorBD.consulta(stmt,cadena); //Realiza la consulta
		
		while (rs.next()) {
			
			textMatricula.setText(rs.getString("matricula"));
			textMarca.setText(rs.getString("marca"));
			textModelo.setText(rs.getString("modelo"));
			textColor.setText(rs.getString("color"));
			textCodigo.setText(rs.getString("codigo"));
			
		}
		rs.close(); //Cierre de la consulta
		
		String tiempo=parking.tiempo(codigo);
		textTiempo.setText(tiempo);
		
		String coste=parking.coste(codigo, parking.obtenerHoraSistema());
		textCoste.setText(coste+"�");

	} //cierre del constructor

	/**
     * Contiene todos los componentes para aparecer en la VentanaPagar.
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Matr\u00EDcula");
		label.setBounds(21, 14, 70, 14);
		contentPane.add(label);
		
		textMatricula = new JTextField();
		textMatricula.setEditable(false);
		textMatricula.setColumns(10);
		textMatricula.setBounds(101, 11, 115, 20);
		contentPane.add(textMatricula);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(21, 42, 70, 14);
		contentPane.add(lblMarca);
		
		textMarca = new JTextField();
		textMarca.setEditable(false);
		textMarca.setColumns(10);
		textMarca.setBounds(101, 39, 115, 20);
		contentPane.add(textMarca);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(21, 70, 70, 14);
		contentPane.add(lblModelo);
		
		textModelo = new JTextField();
		textModelo.setEditable(false);
		textModelo.setColumns(10);
		textModelo.setBounds(101, 67, 115, 20);
		contentPane.add(textModelo);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setBounds(21, 100, 70, 14);
		contentPane.add(lblColor);
		
		textColor = new JTextField();
		textColor.setEditable(false);
		textColor.setColumns(10);
		textColor.setBounds(101, 97, 115, 20);
		contentPane.add(textColor);
		
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(21, 128, 70, 14);
		contentPane.add(lblCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setEditable(false);
		textCodigo.setColumns(10);
		textCodigo.setBounds(101, 125, 115, 20);
		contentPane.add(textCodigo);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setBounds(21, 156, 70, 14);
		contentPane.add(lblTiempo);
		
		textTiempo = new JTextField();
		textTiempo.setEditable(false);
		textTiempo.setColumns(10);
		textTiempo.setBounds(101, 153, 115, 20);
		contentPane.add(textTiempo);
		
		lblCoste = new JLabel("Coste");
		lblCoste.setBounds(21, 184, 70, 14);
		contentPane.add(lblCoste);
		
		textCoste = new JTextField();
		textCoste.setEditable(false);
		textCoste.setColumns(10);
		textCoste.setBounds(101, 181, 115, 20);
		contentPane.add(textCoste);
		
		btnPagar = new JButton("Pagar");
		btnPagar.addActionListener((ActionListener) this);
		btnPagar.setBounds(122, 212, 94, 23);
		contentPane.add(btnPagar);
		
		btnAtras = new JButton("Atras");
		btnAtras.addActionListener((ActionListener) this);
		btnAtras.setBounds(21, 212, 91, 23);
		contentPane.add(btnAtras);

	} //cierre de componentes
	
	/**

     * ActionPerformed responde al evento producido del ActionListener
     * 
     */
	public void actionPerformed(ActionEvent evento) {  //la cabecera del un �nico actionPerformed
		
		if(evento.getSource()==btnPagar){ //Saldr� del parking cuando al bot�n Pagar se invoca el evento asociado.
			
			String codigo=textCodigo.getText().toUpperCase(); //Obtiene la matricula del JTextField convierte en may�scula y se guarda en un String
			
			int piso=parking.toPiso(codigo); //Convierte el codigo al piso de tipo entero
			int plaza=parking.toPlaza(codigo); //Convierte el codigo al plaza de tipo entero
			
			parking.salir(piso, plaza); //Llama al m�todo para que salga el veh�culo de la plaza pasando los par�metros de piso y de plaza. 
			
			parking.archivoLog("","","","","",codigo, "SALIDA"); //Se escribe en el archivo log los siguientes par�metros
			
			
			cadena="UPDATE plazas SET libre='1' WHERE codigo='"+codigo+"';"; //Sentencia de sql para actualizar en la base de datos en la tabla Plazas el c�digo obtenido.
			GestorBD.consultaActualiza(cadena); //Ejecuta la consulta que se le pasa como par�metro
			
			cadena="UPDATE plazas SET h_entrada=? WHERE codigo='"+codigo+"';"; //Se actualiza la hora de la tabla Plazas
			GestorBD.actualizarHora(cadena); //Ejecuta la consulta realizada
			
			cadena="DELETE FROM vehiculo WHERE codigo=?;"; //Se borra la fila de la tabla vehiculo de la plaza adecuada por el c�digo, porque ese Vehiculo ya no existe en nuestro parking.
			GestorBD.eliminar(cadena,codigo); //Ejecuta la consulta 
			
			JOptionPane.showMessageDialog(null, "Has salido correctamente"); //muestra un aviso de que ha salido del parking
			
			this.dispose(); //cerrar la VentanaPagar
			
		} //Cierre del evento asocioado al bot�n Pagar.
		
		if(evento.getSource()==btnAtras){ //Al realizar el evento en el bot�n Atras se ejecutar� la VentanaSalir.
			
			 //Ejecuta la VentanaSalir.
			 VentanaSalir frame4; //Declara la VentanaSalir
			 frame4=new VentanaSalir(); //Se crea la VentanaSalir 
			 frame4.setVisible(true); //Hace visible la VentanaPlaza
			 frame4.setLocationRelativeTo(null); //Localiza en el centro relativo a esta ventana.
			 setVisible(false); //No hace visible la VentanaPagar
			
		} //Cierre del evento asocioado al bot�n Atras

	} //Cierre del m�todo actionPerformed
} //cierre de la clase