package vista;

import gestor.GestorParking;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;

/**
 * Extiende de JFrame implementando ActionListener teniendo los atributos de tipo JButton para acceder a otras ventanas, un atributo de GestorParking y un array bidimensional de JButton para crear las plazas como botones. 
 * 
 * @author SP,EA
 * @see	VentanaEntrar
 * @see VentanaSalir
 * @see VentanaVer
 * @see GestorParking
 */

@SuppressWarnings("serial")
public class VentanaPrincipal extends JFrame implements ActionListener{

	//Atributos de la clase
	private JPanel contentPane;
	private JButton btnEntrar;
	private JButton btnSalir;
	private JButton btnVer;
	private GestorParking parking; //Para poder usar los metodos de GestorParking
	private int filas=3;
	private int columnas=20;
	private JButton[][] botones=new JButton[filas][columnas]; //Array bidimensioanl de botones


	/**
	 * 
     * Constructor sin par�metros y contiene el m�todo componentes
     * 
     */
	public VentanaPrincipal() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Parking");
		setIconImage(new ImageIcon(getClass().getResource("Img/logo.png")).getImage()); //logo de la ventana
		setBounds(100, 100, 197, 219);
		
		componentes();
		
		parking=new GestorParking();
		
		UIManager.getDefaults().put("Button.disabledText",Color.WHITE); //si el bot�n est� deshabilitado, el color del texto ser� blanco
		UIManager.put("Button.select", Color.WHITE); //si el bot�n est� seleccionado establece el color del fondo blanco

	} //Cierre del constructor
	
	/**
     * Contiene todos los componentes para aparecer en la VentanaPrincipal.
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblParking = new JLabel("\u00A1Bienvenido!");
		lblParking.setHorizontalAlignment(SwingConstants.CENTER);
		lblParking.setBounds(43, 11, 89, 14);
		contentPane.add(lblParking);
		
		btnEntrar = new JButton("ENTRAR");
		btnEntrar.addActionListener((ActionListener) this);
		btnEntrar.setBounds(43, 70, 89, 23);
		contentPane.add(btnEntrar);
		
		btnSalir = new JButton("SALIR");
		btnSalir.addActionListener((ActionListener) this);
		btnSalir.setBounds(43, 104, 89, 23);
		contentPane.add(btnSalir);
		
		btnVer = new JButton("VER");
		btnVer.addActionListener((ActionListener) this);
		btnVer.setBounds(43, 138, 89, 23);
		contentPane.add(btnVer);
		
		JLabel lblElijeUnaOpcin = new JLabel("Elije una opci\u00F3n:");
		lblElijeUnaOpcin.setHorizontalAlignment(SwingConstants.CENTER);
		lblElijeUnaOpcin.setBounds(26, 36, 128, 14);
		contentPane.add(lblElijeUnaOpcin);

		
	} //Cierre del m�todo componentes

	/**
	 * 
     * Responde al ActionListener al producir una acci�n.
     * 
     */
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource()==btnEntrar){ //Al hacer clic en el bot�n ENTRAR se produce se realiza lo que hay en las siguientes sentencias.

			try {

				//Hay que comprobar si el Parking est� libre o no.
				if(parking.estaLLeno()==false){

					// Ejecuta la VentanaEntrar.
					VentanaEntrar frame2; //Se declara la VentanaEntrar como frame2
					frame2 = new VentanaEntrar(); //Se crea la VentanaEntrar
					frame2.setVisible(true); //Hace visible la VentanaEntrar
					frame2.setLocationRelativeTo(null); //Localiza en el centro de la pantalla.
					
				}else{

					ImageIcon completo = new ImageIcon("src/vista/Img/completo.png"); //Imagen del parking completo
					//ImageIcon completo = new ImageIcon("C:/Users/agirre/workspace/ProyectoEli/src/vista/Img/completo.png"); //Imagen del parking completo
					JOptionPane.showMessageDialog(null,completo,"Completo",JOptionPane.WARNING_MESSAGE); //Aviso por pantala incluyendo la imagen anteriormente definida.
				}
			
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage()); //Aviso por pantalla por la excepci�n
			}
		}
		
		if(evento.getSource()==btnSalir){ //Al hacer clic en el bot�n SALIR se produce se realiza lo que hay en las siguientes sentencias.

			//Hay que comprobar si el Parking est� vacio o no.
			try {
				if(parking.estaVacio()==false){
					
					// Ejecuta la VentanaSalir.
					VentanaSalir frame4=new VentanaSalir(); //Se crea la VentanaSalir como frame4
					frame4.setVisible(true); //Hace visible la VentanaSalir
					frame4.setLocationRelativeTo(null); //Localiza la VentanaSalir dependiendo de la anterior ventana.
				}
				else {
					ImageIcon vacio = new ImageIcon("src/vista/Img/vacio.png"); //Se define la imagen del parking vacio 
					JOptionPane.showMessageDialog(null,vacio,"Vacio",JOptionPane.WARNING_MESSAGE); //Aviso por pantala incluyendo la imagen
				}
			} catch (SQLException | HeadlessException e) {
				JOptionPane.showMessageDialog(null, e.getMessage()); //Aviso por pantalla por las excepciones
			}
		}
		
		if(evento.getSource()==btnVer){ //Al hacer clic en el bot�n VER se produce se realiza lo que hay en las siguientes sentencias.
		
			try {
				
				if(parking.estaVacio()==false){
					
					// Ejecuta la VentanaVer.
					VentanaVer frame6=new VentanaVer(); //Se crea la VentanaVer como frame6
					frame6.setVisible(true); //Hace visible la VentanaVer
					frame6.setLocationRelativeTo(null); //Localiza la VentanaVer dependiendo de la anterior ventana.
					
				}
				else {
					
					ImageIcon vacio = new ImageIcon("src/vista/Img/vacio.png"); //parking vacio
					JOptionPane.showMessageDialog(null,vacio,"Vacio",JOptionPane.WARNING_MESSAGE);
				}
			} catch (SQLException | HeadlessException e) {
				JOptionPane.showMessageDialog(null, e.getMessage()); //Aviso por pantalla por las excepciones
			}
		}
		
	} //Cierre del m�todo actionPerformed
	
	/**
     * Crea un array de botones bidimensional pasando el par�metro frame para que se carguen los botones en la ventana correspondiente.
     *  
     * @param frame Se carga en la ventana correspondiente.
     * @return JButton[][] Devuelve un array bidimensional de botones.
     *  
     */
	public JButton[][] crearBotones(JFrame frame){
		
		//Se recorre el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Se recorre el array por cada plaza
			for(int j=0;j<columnas;j++){

				botones[i][j]=new JButton(); //Se crea un JButton para cada posici�n del array
				botones[i][j].setBackground(Color.GREEN); //Se establece el fondo de los botones de color Verde
				botones[i][j].setSize(10, 10); //El tama�o de cada JButton
				botones[i][j].setBounds((j+1)*60, (i+1)*70, 55, 65); //La posici�n de cada JButton dependiendo del espacio entre ellos. 
				botones[i][j].setText(parking.mostrarCodigo(i, j)); //Muestra el c�digo de cada plaza en cada JButton

				//se a�ade cada boton a la ventana correspondiente
				frame.getContentPane().add(botones[i][j]);

			}
		}
		frame.setSize(1290, 391); //establece el tama�o de la ventana
        return botones;
    }//Cierre del m�todo crearBotones
	
} //Cierre de la clase 