package vista;

import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;

import modelo.Marca;
import modelo.Modelo;
import modelo.Tipo;
import modelo.Vehiculo;
import gestor.GestorBD;

import java.awt.Color;


/**
 * Extiende del JFrame implementando KeyListener y ActionListener, teniendo los atributos de tipo JButton, JComboBox y de otras clases como Tipo, Marca, Modelo y Vehiculo.
 * 
 * @author SP,EA
 * @see	VentanaPlaza
 * @see Tipo
 * @see Marca
 * @see Modelo
 * @see Vehiculo
 * @see GestorBD
 */

@SuppressWarnings("serial")
public class VentanaEntrar extends JFrame implements KeyListener,ActionListener{

	//Atributos de la clase
	private JPanel contentPane;
	@SuppressWarnings("unused")
	private Vehiculo vehiculo; //declaramos la clase Vehiculo
	private Tipo tip; //declaramos la clase Tipo
	private Marca marca; //declaramos la clase Marca
	private Modelo modelo; //declaramos la clase Modelo
	private JComboBox<Tipo> cmbBTipo; //muestra la lista de Tipos de veh�culos de la bases de datos
	private JComboBox <Marca> cmbBMarca; //muestra la lista de la bases de datos
	private JComboBox <Modelo> cmbBModelo; //muestra la lista de la bases de datos
	private JComboBox <Object> cmbBColor; //muestra la lista de array de colores.
	private String[] listaColores = {"NEGRO","BLANCO","ROJO","MARRON","BEIGE","AMARILLO","CER�LEO","VERDE OLIVA"}; //Lista de array para combobox
	private static JTextField textMatricula;
	private static Statement stmt;
	private static ResultSet rs;
	private String cadena;
	private static String matricula;
	private String nombre;
	private String codigo_marca;
	private String tipo;
	private String mod;
	private String marc;
	private String color;
	private JButton btnAceptar;
	private JButton btnSalir;
	private JLabel lblMatrcula;
	private JLabel lblTipo;
	private JLabel lblMarca;
	private JLabel lblModelo;
	private JLabel lblColor;
	private JLabel lblEntrar;

	/**
     * Constructor sin par�metros, aparecen todos los componentes y crea la conexi�n con la base de datos.
	 * @throws SQLException Contiene informaci�n sobre un error de acceso a la base de datos.
     * 
     */
	public VentanaEntrar() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Datos del veh�culo");
		setIconImage(new ImageIcon(getClass().getResource("Img/vehiculo.png")).getImage());
		setBounds(100, 100, 284, 294);
		
		componentes();
		
		GestorBD.conectar();
		stmt=GestorBD.conexion();

	} //Cierre del constructor
	
	/**
     * Contiene todos los componentes para aparecer en la VentanaEntrar.
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblMatrcula = new JLabel("Matr\u00EDcula");
		lblMatrcula.setBounds(33, 65, 70, 14);
		contentPane.add(lblMatrcula);

		lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(33, 90, 46, 14);
		contentPane.add(lblTipo);
		
		lblColor = new JLabel("Color");
		lblColor.setBounds(33, 166, 46, 14);
		contentPane.add(lblColor);
		
		lblMarca = new JLabel("Marca");
		lblMarca.setBounds(33, 116, 46, 14);
		contentPane.add(lblMarca);
		
		lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(33, 141, 46, 14);
		contentPane.add(lblModelo);
		
		textMatricula = new JTextField();
		textMatricula.addKeyListener(this);
		textMatricula.setBounds(113, 62, 115, 20);
		contentPane.add(textMatricula);
		textMatricula.setColumns(10);
		
		cmbBTipo = new JComboBox<Tipo>();
		cmbBTipo.addActionListener((ActionListener) this);
		cmbBTipo.setEnabled(false);
		cmbBTipo.setBounds(113, 90, 115, 20);
		contentPane.add(cmbBTipo);

		cmbBMarca = new JComboBox <Marca> ();
		cmbBMarca.addActionListener((ActionListener) this);
		cmbBMarca.setEnabled(false);
		cmbBMarca.setBounds(113, 116, 115, 20);
		contentPane.add(cmbBMarca);
		
		cmbBModelo = new JComboBox <Modelo> ();
		cmbBModelo.addActionListener((ActionListener) this);
		cmbBModelo.setEnabled(false);
		cmbBModelo.setBounds(113, 141, 115, 20);
		contentPane.add(cmbBModelo);
		
		cmbBColor = new JComboBox <Object> (listaColores);
		cmbBColor.addActionListener((ActionListener) this);
		cmbBColor.setEnabled(false);
		cmbBColor.setBounds(113, 166, 115, 20);
		contentPane.add(cmbBColor);
		
		lblEntrar = new JLabel("Rellena los datos:");
		lblEntrar.setBounds(113, 25, 145, 14);
		contentPane.add(lblEntrar);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener((ActionListener) this);
		btnAceptar.setEnabled(false);
		btnAceptar.setBounds(113, 209, 115, 23);
		contentPane.add(btnAceptar);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener((ActionListener) this);
		btnSalir.setBounds(14, 209, 89, 23);
		contentPane.add(btnSalir);
		
				
	} //Cierre del m�todo componentes

	/**
     * Sirve para comprobar si la matricula antigua es correcta.
     * @param matricula Es un String que se recoge del textField. 
     * @return boolean Retorna un booleano dependiendo de la condicion de la condicicon para saber si la estructura de la matricula es correcta. Ejemplo: VI4562F
     * 
     */
	public static boolean validarMatAntigua(String matricula){
		 
		 boolean correcta=true, correcta1 = true, correcta2 = true;
		 
		 try{
			 
			 correcta=esCaracter(matricula.substring(0,1))&&esCaracter(matricula.substring(1,2));
			 String numericaAnt = matricula.substring(2, 6);
			 correcta1 = validarNumerica(numericaAnt);
		 
			 if (!correcta)
				 JOptionPane.showMessageDialog(null,"Los dos primeros caracteres deben ser letras");
			 if (!correcta1)
					JOptionPane.showMessageDialog(null, "Las posiciones 3, 4, 5 y 6 deben ser n�meros.");
			 	 char ultPos = matricula.charAt(6);
			 	 correcta2 = validarLetra(ultPos);
			 
			 if (!correcta2)
					JOptionPane.showMessageDialog(null, "La �ltima letra no puede ser n�merica ni vocal ni '�' ni 'Q'.");
			 
		 } catch (StringIndexOutOfBoundsException e){
			 e.getMessage();
		 }
		 
		 return correcta && (correcta1 && correcta2);
	 } 
	
	/**
     * Sirve para comprobar si la matricula europea es correcta.
     * @param matricula Es un String que se recoge del textField. 
     * @return boolean Retorna un booleano para saber si la estructura de la matricula es correcta. Ejemplo: 4485BDE
     * 
     */
	 public static boolean validarMatEuropea(String matricula){
		 
		 boolean correcta1=true, correcta2=true;
		 
		 try{
			 
			 String numericaEurop = matricula.substring(0, 4);
			 correcta1 = validarNumerica(numericaEurop);
			 if (!correcta1)
				 JOptionPane.showMessageDialog(null,"Los cuatro primeros carecteres deben ser num�ricas.");
			 	correcta2=validarLetra(matricula.charAt(4))&&validarLetra(matricula.charAt(5)) && validarLetra(matricula.charAt(6));
			 	
			 if (!correcta2)
				 JOptionPane.showMessageDialog(null,"Los tres �ltimos caracteres alfab�ticas no pueden contener n�meros ni vocales ni '�' ni 'Q'.");
		 }
		 catch (StringIndexOutOfBoundsException e){
			 e.getMessage();
		 }
	 	return (correcta1 && correcta2);
	 } 
	 
	 /**
     * Si es verdadero o falso que es un n�mero y no un car�cter.
     * @param num Es un String obtenida de los m�todos para validar la matricula. 
     * @return boolean Retorna un booleano para saber si es un n�mero.
     * 
     */
	 private static boolean validarNumerica(String num){
		 
		 boolean correcta = true;
		 int longitud = num.length();
		 int i=0;
		 
		 while( i <= longitud-1 && correcta == true){
			 
			 char numero = num.charAt(i);
			 if ( numero<'0' || numero>'9'){
				 correcta = false;
			 }
			 
			 i++;
		 }
	 return correcta;
	 }
	 
	/**
     * Si es verdadero o falso que es un car�cter y no un n�mero, y si no es ninguna vocal, ni � ni Q.
     * @param letra Es un char obtenida de los m�todos para validar la matricula. 
     * @return boolean Retorna un booleano para saber si es un caracter adecuado.
     * 
     */
	 private static boolean validarLetra (char letra){
		 
		 return (letra != 'A' && letra != 'E' && letra != 'I' && letra != 'O' && letra != 'U' && letra != '�' && letra != 'Q' && letra >= 'A' && letra <='Z');
	 }

	/**
     * Si es verdadero o falso que es un caracter y no un n�mero.
     * @param cadena Es un String que se recoge del subString del textField. 
     * @return boolean Retorna un booleano para saber si es un caracter.
     * 
     */
	public static boolean esCaracter(String cadena){
		
		boolean esCaracter=false;
		
		for(int i=0;i<cadena.length();i++){
			if((cadena.charAt(i)>64 && cadena.charAt(i)<91)){
				esCaracter=true;
			}
		}
		return esCaracter;	
	}
	
	/**
     * Para comprobar si s�lo existe esa �nica matricula en la tabla Vehiculo
     * @throws SQLException
     * @return boolean. Retorna un booleano para saber si la existe de la matricula.
     * 
     */
	public static boolean vehiculoExiste() throws SQLException{
		 
		 boolean encontrado=false;

		 String cadena = "SELECT * FROM parking.vehiculo;";
		 rs = GestorBD.consulta(stmt,cadena); //devuelve el resultado
		 
		 while (rs.next()){
			 
			 if(rs.getString("matricula").equals(matricula)){
				 encontrado=true;
			 }
		 }
		 return encontrado;
		 
	 }

	/**
     * KeyPressed responde al KeyListener cuando el usuario pulsa una tecla.
     * 
     */
	public void keyPressed(KeyEvent key) {}

	/**
     * KeyReleased responde al KeyListener cuando el usuario suelta una tecla.
     * 
     */
	public void keyReleased(KeyEvent key) {
	
		if(key.getSource()==textMatricula){
			
			try{
				//Validar los campos textFields
				if(textMatricula.getText().equals("")){
					
					JOptionPane.showMessageDialog(textMatricula, "Introduce la matricula","ERROR", JOptionPane.WARNING_MESSAGE);
				}
				
				matricula=textMatricula.getText().toUpperCase(); //obtiene la matricula del JTextField.
				boolean valida=false;

				if(matricula.length()>6){
					
					if (esCaracter(matricula.substring(0,1))){
						valida=validarMatAntigua(matricula);
					}
					else{
						valida=validarMatEuropea(matricula);
					}
			
				}


				if(valida==true){
					
					//Se realiza la consulta para saber si la matricula es �nica en la base de datos y en el array
					if(vehiculoExiste()==true){
						JOptionPane.showMessageDialog(null, "La matricula ya existe.");
						textMatricula.setText("");

					}
					else{
						cmbBTipo.setEnabled(true);
						textMatricula.setEnabled(false);
						btnSalir.setVisible(false);
						
						//Cuando haya validado la matricula el combobox del tipo se activa. Y se hace la consulta.
						cadena = "SELECT * FROM tipo;"; //SQL en un String
						rs = GestorBD.consulta(stmt,cadena); //Devuelve el resultado
												
						while (rs.next()) { //Mientras que haya datos
						//Obtiene el codigo de la tabla Tipo.
						tipo=rs.getString("codigo"); 
						//Se crea un nuevo objeto pasando par�metro el c�digo obtenido por un String.
						tip=new Tipo(tipo); 
						//Se a�ade al JComboBox como �tem.
						cmbBTipo.addItem(tip); //se muestra el m�todo toString de la clase Tipo.

	
						}
						rs.close(); //Cierre de la consulta
					}
				}
			}
			catch (StringIndexOutOfBoundsException e){
				 e.getMessage();
				 cmbBTipo.setEnabled(false);
			}
			catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			} catch (Exception ex) {
				ex.getMessage();
			}
		}
	}

	/**
     * KeyTyped responde al KeyListener cuando el usuario pulsa y suelta una tecla.
     * 
     */
	public void keyTyped(KeyEvent key) {}
	
	/**
     * ActionPerformed responde al evento producido del ActionListener
     * 
     */
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource()==cmbBTipo){ //Al realizar el evento en el primer JComboBox 
			
			tipo=cmbBTipo.getSelectedItem().toString(); //Guardar un item del combobox en el String llamado tipo
			
			if(cmbBTipo.isValid()==true){ //si es v�lido JComboBox
				
				cmbBMarca.setEnabled(true); //activamos el JComboBox 
			    
				try {
					
					cadena = "SELECT * FROM parking.marca WHERE tipo='"+tipo+"';"; //la sentencia sql se guarda en un String
					rs = GestorBD.consulta(stmt,cadena); //Realiza la consulta
				
					while (rs.next()) {
						
						codigo_marca=rs.getString("codigo"); //muestra el codigo de la tabla marca
						nombre=rs.getString("nombre"); //obtiene y muestra el nombre
						
						marca=new Marca(codigo_marca,nombre); //a�adimos al objeto Marca pasando todos los par�metros.
						cmbBMarca.addItem(marca); //a�adimos al JComboBox el nombre del veh�culo elegido. Como itams se muestra el m�todo toString de la clase Marca.
					}
					rs.close(); //Cierre de la consulta

				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		}
		
		if(evento.getSource()==cmbBMarca){ //Cuando el evento se realiza en el segundo JComboBox 
		
			cmbBTipo.setEnabled(false); //desactivamos el JComboBox
			marc=cmbBMarca.getSelectedItem().toString(); //Guardar un item del combobox en el String llamado marc

			if(cmbBMarca.isValid()==true){ //si es v�lido JComboBox
				
				cmbBModelo.setEnabled(true); //activamos el JComboBox 
				
				try {
				    
					cadena = "SELECT * FROM parking.marca WHERE nombre='"+marc+"';"; //la sentencia consulta de sql se guarda en un String
					rs = GestorBD.consulta(stmt,cadena); //Realiza la consulta
				
					while (rs.next()) {
						codigo_marca=rs.getString("codigo"); //muestra el codigo de la tabla marca
					}
					rs.close();

					cadena = "SELECT * FROM parking.modelo WHERE marca='"+codigo_marca+"';"; //la sentencia consulta de sql se guarda en un String

					rs = GestorBD.consulta(stmt,cadena); //Realiza la consulta
					String marca=""; //Un String nulo
				
					while (rs.next()) {
						
						mod=rs.getString("modelo"); //obtiene el codigo de la tabla marca
						marca=rs.getString("marca"); //obtiene el nombre de la tabla marca
						
						modelo=new Modelo(mod,marca); //a�adimos al objeto Modelo pasando todos los par�metros.

						cmbBModelo.addItem(modelo); //a�adimos al JComboBox el modelo del veh�culo elegido. Como itams se muestra el m�todo toString de la clase Modelo.
					}
					rs.close(); //Cierre de la consulta
					
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		}
		
		if(evento.getSource()==cmbBModelo){ //Cuando el evento se realiza en el tercer JComboBox 
			
			cmbBMarca.setEnabled(false); //desactivamos el JComboBox 
			mod=cmbBModelo.getSelectedItem().toString(); //Guardar un item del JComboBox en el String llamado mod

			if(cmbBModelo.isValid()){ //si es v�lido JComboBox
				
				cmbBColor.setEnabled(true); //activamos el JComboBox 
				cmbBModelo.setEnabled(false); //desactivamos el JComboBox 
			}
			
		}
		
		if(evento.getSource()==cmbBColor){ //Cuando el evento se realiza en el cuarto JComboBox 

			cmbBModelo.setEnabled(false); //desactivamos el JComboBox 
			color=cmbBColor.getSelectedItem().toString(); //Guardar un item del JComboBox en el String llamado mod
			cmbBColor.setEnabled(false); //desactivamos el JComboBox 
			btnAceptar.setEnabled(true); //activamos el bot�n Aceptar
		}
		
		if(evento.getSource()==btnAceptar){ //Al realizar el evento en el bot�n Aceptar, se guardarn los datos obtenidos.
			
			if(evento.getSource()==btnAceptar){

					String matricula=textMatricula.getText().toUpperCase(); //Obtiene la matricula en may�scula
					int valor1=0; //Se define el valor del entero para recoger la respuesta de la confirmaci�n de la matr�cula

					valor1=JOptionPane.showConfirmDialog(null,"�Es correcta la matricula? \n      "+matricula,"Seleccione una opci�n", JOptionPane.YES_NO_OPTION); //Se confirma la matr�cula			
					
					if(valor1==0){ //Si la respuesta es "Si", ejecuta la VentanaPlaza
						
						//Ejecuta la VentanaPlaza
						VentanaPlaza frame3; //declarar la VentanaPlaza
						frame3 = new VentanaPlaza(matricula,tipo,marc,mod,color); //Pasa a la VentanaPlaza los par�metros recogidos del TextField y de los JComboBox.
						this.dispose(); //Cierre de la VentanaEntrar
						frame3.setVisible(true); //Hace visible la VentanaPlaza
						frame3.setLocationRelativeTo(null); //Localiza en el centro relativo a esta ventana.

					}
					else if(valor1==1){ //Si la respuesta es "No", desaperecer�n los JComboBox y las labels adecuadas para que s�lo pueda modificar la matr�cula.

						textMatricula.setEnabled(true);
						cmbBTipo.setVisible(false);
						cmbBMarca.setVisible(false);
						cmbBModelo.setVisible(false);
						cmbBColor.setVisible(false);
						lblTipo.setVisible(false);
						lblMarca.setVisible(false);
						lblModelo.setVisible(false);
						lblColor.setVisible(false);
						lblEntrar.setText("Modifica la matr�cula:");
						
						String mat=textMatricula.getText().toUpperCase().trim(); //Se obtiene la matr�cula modificada
						
						if(mat.length()>6){ //Eval�a s�lo los 7 car�cteres
							if (esCaracter(mat.substring(0,2))){ //si los dos primeros caracteres son letras ser� la matricula antigua
								validarMatAntigua(mat); //Si es correcta no mostrar� ning�n mensaje y el usuario podr� dar al bot�n aceptar
							}
							else{ //Sino la europea
								validarMatEuropea(mat); //Si es correcta no mostrar� ning�n mensaje y el usuario podr� dar al bot�n aceptar
							}
					
					}
				}
			}
		}
		
		if(evento.getSource()==btnSalir){ //Al realizar el evento en el bot�n Salir saldr� de esta VentanaEntrar.
			this.dispose(); //Cierra la VentanaEntrar
		}
		
	} //Cierre del m�todo actionPerformed
} //Cierre de la clase 