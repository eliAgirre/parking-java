package vista;

import gestor.GestorBD;
import gestor.GestorParking;

import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

/**
 * Extiende de la clase JFrame implementando ActionListener y muestra los datos de la plaza (datos del veh�culo, tiempo, coste) obteniendo el codigo de la VentanaVer.
 *  
 * @author SP,EA
 * @see VentanaVer
 * @see GestorParking
 * @see	GestorBD
 * @see VentanaPrincipal
 */

@SuppressWarnings("serial")
public class VentanaDatosPlaza extends JFrame implements ActionListener{

	//Atributos de la clase
	private JPanel contentPane;
	private JTextField textCodigo;
	private JButton btnAtras;
	private JButton btnSalir;
	private JLabel lblMatricula;
	private JTextField textMatricula;
	private JLabel lblMarca;
	private JTextField textMarca;
	private JLabel lblColor;
	private JTextField textColor;
	private JLabel lblCodigo;
	private JTextField textTiempo;
	private JTextField textCoste;
	private JLabel lblTiempo;
	private JLabel lblCoste;
	private JLabel lblModelo;
	private JTextField textModelo;
	private GestorParking parking;
	private String cadena;
	private static ResultSet rs;
	private static Statement stmt;


	/**
     * Constructor como par�metro codigo obtenido desde la VentanaVer.
     * @param codigo Es un String llamado codigo de una plaza espec�fica.
	 * @throws SQLException 
     */
	public VentanaDatosPlaza(String codigo) throws SQLException {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Datos de la plaza");
		setIconImage(new ImageIcon(getClass().getResource("Img/plaza.png")).getImage());
		setBounds(100, 100, 259, 313);
		
		componentes();
		
		parking=new GestorParking();
		
		textCodigo.setText(codigo); //Establece el texto con el c�digo obtenido de la VentanaVer
		
		GestorBD.conectar(); //Conecta con la base de datos
		stmt=GestorBD.conexion(); //Establece la conexi�n 
		
		cadena = "SELECT * FROM vehiculo WHERE codigo='"+codigo+"';"; //la sentencia sql se guarda en un String
		rs = GestorBD.consulta(stmt,cadena); //Realiza la consulta
		
		while (rs.next()) {
			
			textMatricula.setText(rs.getString("matricula")); //Establece la matricula obtenida de la base de datos y muestra en el JTextField
			textMarca.setText(rs.getString("marca")); //Establece la marca obtenida de la base de datos y muestra en el JTextField
			textModelo.setText(rs.getString("modelo"));  //Establece el modelo obtenida de la base de datos y muestra en el JTextField
			textColor.setText(rs.getString("color")); //Establece el color obtenida de la base de datos y muestra en el JTextField
			
		}
		rs.close(); //Cierre de la consulta
		
		String tiempo=parking.tiempo(codigo); //Obtiene el tiempo del establecimiento del vehiculo desde que ha entrado al parking
		textTiempo.setText(tiempo); //Establece el tiempo y muestra el en JTextField
		
		String coste=parking.coste(codigo, parking.obtenerHoraSistema()); //Obtiene el coste dependiendo del tiempo transcurrido el vehiculo en esta plaza.
		textCoste.setText(coste+"�"); //Establece el coste y muestra el en JTextField
		
	} //Cierre del constructor

	/**
     * Contiene todos los componentes para aparecer en la VentanaDatosPlaza.
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblMatricula = new JLabel("Matr\u00EDcula");
		lblMatricula.setBounds(20, 32, 70, 14);
		contentPane.add(lblMatricula);
		
		textMatricula = new JTextField();
		textMatricula.setEditable(false);
		textMatricula.setColumns(10);
		textMatricula.setBounds(98, 29, 117, 20);
		contentPane.add(textMatricula);
		
		lblMarca = new JLabel("Marca");
		lblMarca.setBounds(20, 60, 70, 14);
		contentPane.add(lblMarca);
		
		textMarca = new JTextField();
		textMarca.setEditable(false);
		textMarca.setColumns(10);
		textMarca.setBounds(98, 57, 117, 20);
		contentPane.add(textMarca);
		
		lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(20, 91, 70, 14);
		contentPane.add(lblModelo);
		
		textModelo = new JTextField();
		textModelo.setEditable(false);
		textModelo.setColumns(10);
		textModelo.setBounds(98, 88, 117, 20);
		contentPane.add(textModelo);
		
		lblColor = new JLabel("Color");
		lblColor.setBounds(20, 119, 70, 14);
		contentPane.add(lblColor);
		
		textColor = new JTextField();
		textColor.setEditable(false);
		textColor.setColumns(10);
		textColor.setBounds(98, 116, 117, 20);
		contentPane.add(textColor);
		
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(20, 147, 70, 14);
		contentPane.add(lblCodigo);
		
		textCodigo = new JTextField();
		textCodigo.setEnabled(false);
		textCodigo.setBounds(98, 144, 117, 20);
		contentPane.add(textCodigo);
		textCodigo.setColumns(10);
	
		btnAtras = new JButton("Atras");
		btnAtras.addActionListener((ActionListener) this);
		
		lblTiempo = new JLabel("Tiempo");
		lblTiempo.setBounds(20, 178, 70, 14);
		contentPane.add(lblTiempo);
		
		lblCoste = new JLabel("Coste");
		lblCoste.setBounds(20, 209, 70, 14);
		contentPane.add(lblCoste);
		
		textCoste = new JTextField();
		textCoste.setEditable(false);
		textCoste.setColumns(10);
		textCoste.setBounds(98, 206, 117, 20);
		contentPane.add(textCoste);
		
		textTiempo = new JTextField();
		textTiempo.setEditable(false);
		textTiempo.setColumns(10);
		textTiempo.setBounds(98, 175, 117, 20);
		contentPane.add(textTiempo);
		btnAtras.setBounds(20, 237, 89, 23);
		contentPane.add(btnAtras);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener((ActionListener) this);
		btnSalir.setBounds(126, 237, 89, 23);
		contentPane.add(btnSalir);
		
	} //Cierre del m�todo componentes
	
	/**
     * ActionPerformed responde al evento producido del ActionListener
     * 
     */
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource()==btnAtras){ //Al realizar el evento en el bot�n Salir cerrar� la VentanaVehiculo.
			
			 VentanaVer frame6; //Declara la VentanaVer como frame6
			 frame6=new VentanaVer(); //Instancia la VentanaVer
			 frame6.setVisible(true); //Hace visible la ventana
			 frame6.setLocationRelativeTo(null); //Localiza la ventana dependiendo la localizaci�n de la VentanaVer
			 setVisible(false); //Hace invisible la VentanaDatosPlaza
			
		} //Cierre del evento asocioado al bot�n Atras
		
		if(evento.getSource()==btnSalir){ //Al realizar el evento en el bot�n Salir cerrar� la VentanaVehiculo.
			
			this.dispose(); //Cierre de la VentanaDatosPlaza
			
		}  //Cierre del evento asocioado al bot�n Salir
		
	} //Cierre del m�todo actionPerformed
} //Cierre de la clase 