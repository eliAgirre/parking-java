package vista;

import modelo.Vehiculo;
import gestor.GestorBD;
import gestor.GestorParking;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

/**
 * Extiende de la clase JFrame implementando MouseListener, y aparece el parking para que el usuario pueda seleccionar la plaza que est� habilitadas en verde para aparcar.
 *  
 * @author SP,EA
 * @see Vehiculo
 * @see GestorParking
 * @see	GestorBD
 * @see VentanaPrincipal
 */

@SuppressWarnings("serial")
public class VentanaPlaza extends JFrame implements MouseListener{

	//Atributos de la clase
	private JPanel contentPane;
	@SuppressWarnings("unused")
	private Vehiculo vehiculo; //Declaraci�n de la clase Vehiculo
	private String matricula; 
	private String tipo;
	private String marca;
	private String modelo;
	private String color;
	private GestorParking parking; //Para poder usar los metodos de GestorParking
	private int filas=3;
	private int columnas=20;
	private JButton[][] botones; //Array bidimensional declarado

	/**
     * Constructor con los par�metros de la VentanaEntrar.
     * 
     * @param matricula String matricula del Vehiculo
     * @param tipo String tipo de Vehiculo
     * @param marca String marca, un atributo del Vehiculo
     * @param modelo String modelo que define el modelo del Vehiculo
     * @param color String color que es una caracter�stica del Vehiculo
     * 
     */
	public VentanaPlaza(String matricula, String tipo, String marca, String modelo, String color) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Selecciona la plaza");
		setIconImage(new ImageIcon(getClass().getResource("Img/plaza.png")).getImage());
		setBounds(100, 100, 451, 302);
		
		componentes();

		this.matricula=matricula; //Establece la matricula al atributo de la clase de tipo String matricula
		this.tipo=tipo; //Establece el tipo al atributo de la clase
		this.marca=marca; //Establece la marca al atributo de la clase de tipo String marca
		this.modelo=modelo; //Establece el modelo al atributo de la clase
		this.color=color;  //Establece el color al atributo de la clase
		
		parking=new GestorParking(); 

		//Llamamos a la ventana principal
		VentanaPrincipal frame1 = new VentanaPrincipal();
		
		//frame1.crearLabels(this);
		botones=frame1.crearBotones(this);
		
		seleccionarPlaza(); //Metodo para la definici�n de cada bot�n. En este caso las plazas deshabilitadas ser�n rojas.

				
	} //Cierre del constructor
	
	/**
     * Contiene todos los componentes para aparecer en la VentanaPlaza.
     *  
     */
	public void componentes(){
		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(248, 248, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label_0 = new JLabel("0");
		label_0.setBounds(21, 101, 21, 14);
		contentPane.add(label_0);
		
		JLabel label_1 = new JLabel("1");
		label_1.setBounds(21, 173, 21, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("2");
		label_2.setBounds(21, 241, 21, 14);
		contentPane.add(label_2);

		JLabel lblSeleccionaUnaPlaza = new JLabel("Selecciona una plaza");
		lblSeleccionaUnaPlaza.setBounds(21, 34, 146, 14);
		contentPane.add(lblSeleccionaUnaPlaza);
	
	} //Cierre del m�todo componentes
	
	/**
     * Carga los botones, si est� deshabilitados ser�n rojas y no se podr� seleccionar. Si est�n habilitados ser�n verdes y se podr�n seleccionar.
     *  
     * @return JButton[][] Devuelve un array bidimensional de botones definidos por el color.
     *  
     */
	public JButton[][] seleccionarPlaza(){
		
		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){
		
				if(parking.libre(i,j)==false){ //Cuando la plaza este ocupada
					botones[i][j].setBackground(Color.RED); //Establece el color del fondo del bot�n rojo
					botones[i][j].setEnabled(false); //Deshabilita el bot�n
				}
				else{ //Si la plaza esta libre
					botones[i][j].setEnabled(true); //Habilita el bot�n
				}
				
				//eventos asociados a cada bot�n
				botones[i][j].addMouseListener((MouseListener) this);
				botones[i][j].addActionListener(new ActionListener() {

				   public void actionPerformed(ActionEvent event) {

			    	//Recorremos el array por cada piso
					for(int i=0;i<filas;i++){
						
						//Recorremos el array por cada plaza
						for(int j=0;j<columnas;j++){
							
							 if (event.getSource()==botones[i][j]){  //Evento asociado a cada bot�n del array bidimensional
								
								 String codigo=botones[i][j].getText(); //Obtiene el c�digo del bot�n asociado
								 
								 vehiculo=new Vehiculo(matricula,tipo,marca,modelo,color,codigo); //Se crea el objeto Vehiculo pasando los par�metros 
								 
								 String evento =  "ENTRADA  //"; //Se crea un String para el archivo log
								 
								 parking.archivoLog(matricula,tipo,marca,modelo,color,codigo, evento);  //Llama al m�todo pasando los par�metros obtenidos del vehiculo y el String establecido
								 
								 int piso = parking.toPiso(codigo); //Convierte el codigo String en un atributo de tipo entero llamado piso 
								 int plaza = parking.toPlaza(codigo); //Convierte el codigo String en un entero

								 parking.aparcar(piso,plaza); //Llama al m�todo aparcar pasando los par�metro antes convertidos en enteros para aparcar el vehiculo en la plaza correspondiente del parking
								 
								 String cadena="INSERT INTO vehiculo VALUES ('"+matricula+"','"+tipo+"','"+marca+"','"+modelo+"','"+color+"','"+codigo+"');"; //Sentencia de sql para insertar datos en la tabla vehiculo
								 GestorBD.consultaActualiza(cadena); //Ejecuta la actualizaci�n
									
								 cadena="UPDATE parking.plazas SET libre='"+0+"',h_entrada='"+parking.obtenerHoraSistema()+"' WHERE codigo='"+codigo+"';"; //Sentencia de sql para actualizar la plaza con el codigo corespondiente y la hora del sistema obtenida en la tabla plazas
								 GestorBD.consultaActualiza(cadena); //Ejecuta la actualizaci�n
									
								 JOptionPane.showMessageDialog(null, "Tu c�digo es:  " +codigo); //Aviso por pantalla sobre el c�digo de la plaza escogida

								 
								 dispose(); //cierra la VentanaPlaza
				             }
						}
					}
			
				   }			
				 
			});
			}
		}
		return botones; //Devuelve el array bidimensional de botones
		
	}

	/**
	 * 
     * Responde al MouseListener cuando el rat�n hace un clic en el bot�n
     * 
     */
	public void mouseClicked(MouseEvent mouse) {} //Cierre del m�todo mouse clicked

	/**
	 * 
     * Responde al MouseListener cuando el rat�n pasa por encima del bot�n
     * 
     */
	public void mouseEntered(MouseEvent mouse) {

		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){
				
				if (mouse.getSource()==botones[i][j]){ //Al pasar por encima de un bot�n correspondido
	
					if(parking.libre(i,j)==false){ //Si la plaza esta ocupada
						botones[i][j].setBackground(Color.RED); //Establece el color del bot�n rojo
						botones[i][j].setEnabled(false); //Deshabilita el bot�n
					}
					else{ //Si la plaza esta libre
						botones[i][j].setBackground(Color.BLUE); //Establece el color del fondo del bot�n azul
						botones[i][j].setForeground(Color.WHITE);  //Establece el color del texto del bot�n blanco
					}
	
				}
			}
		}
	} //Cierre del m�todo mouse entered

	/**
	 * 
     * Responde al MouseListener cuando el rat�n sale del bot�n
     * 
     */
	public void mouseExited(MouseEvent mouse) {
	
		//Recorremos el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Recorremos el array por cada plaza
			for(int j=0;j<columnas;j++){
				
				if (mouse.getSource()==botones[i][j]){  //Al salir de un bot�n correspondido
					
					if(parking.libre(i,j)==true){ //Si la plaza esta libre
						botones[i][j].setBackground(Color.GREEN); //Establece el color del fondo del bot�n verde
						botones[i][j].setForeground(Color.BLACK); //Establece el color del texto del bot�n negro
						botones[i][j].setEnabled(true); //Habilita el bot�n
					}
					else{ //Si la plaza esta ocupada
						botones[i][j].setEnabled(false); //Deshabilita el bot�n
			
					}
				}
			}
		}
	} //Cierre del m�todo mouse exited

	/**
	 * 
     * Responde al MouseListener cuando el rat�n presiona el bot�n
     * 
     */
	public void mousePressed(MouseEvent mouse) {
		
		//Se recorre el array por cada piso
		for(int i=0;i<filas;i++){
			
			//Se recorre  el array por cada plaza
			for(int j=0;j<columnas;j++){
				
				//Al presionar de un bot�n correspondido
				if (mouse.getSource()==botones[i][j]){ 
					//Establece el color del texto del bot�n negro
					botones[i][j].setForeground(Color.BLACK); 
				}
			}
		}
	} //Cierre del m�todo mouse pressed

	/**
	 * 
     * Responde al MouseListener cuando el rat�n presiona y suelta el bot�n
     * 
     */
	public void mouseReleased(MouseEvent mouse) {} //Cierre del m�todo mouse released
	
} //Cierre de la clase 
