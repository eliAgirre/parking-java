package gestor;

import modelo.Plaza;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*; 

import javax.swing.*;

/**
 * Se conecta con la base de datos, crea un array bidimensional, devuelve la informaci�n del estado de una plaza concreta, es posible saber el estado del parking, y el tiempo y el coste de una plaza mediante el codigo de la plaza. 
 * 
 * @author SP,EA
 * @see Plaza
 */

public class GestorParking {
	
	//Atributos de la clase
	private int filas=3, columnas=20;
	private Plaza[][] array; //array bidimensional (3 filas, 20 columnas)
	private Plaza plazaArray;
	private boolean libre;
	private Timestamp ts;
	private String codigo;
	private double coste;
	private int piso;
	private int plaza;
	private Timestamp h_entrada;
	private Statement stmt=null;
	
	
	/**
     * Constructor sin par�metros que se conecta con la bases de datos y carga el array bidimensional.
     * 
     */
	public GestorParking() {
		
		try {
			
			//array bidimensional (3 filas, 20 columnas)
			array=new Plaza [filas][columnas];
			
			//Hace la conexi�n con la base de datos
			GestorBD.conectar();
			
			//Establece la conexi�n con la base de datos 
			Statement stmt = GestorBD.conexion();
			
			//Se crea la sentencia sql en una variable "cadena" de tipo String
			String cadena = "SELECT * FROM plazas";
			
			//Obtiene el resultado de la base de datos
			ResultSet rs = GestorBD.consulta(stmt, cadena);

			//Recorremos el array por cada piso
			for(int i=0;i<filas;i++){
						
				//Recorremos el array por cada plaza
				for(int j=0;j<columnas;j++){
						
						//miramos que haya algo en el ResultSet
						rs.next();

							//Cargamos los datos en cada atributo
							codigo = rs.getString("codigo");
							piso = rs.getInt("piso");
							plaza = rs.getInt("plaza");
							libre=rs.getBoolean("libre");
							h_entrada = null;
							
							//Creamos la plaza con sus atributos extraidos de la BD
							plazaArray = new Plaza(codigo, piso, plaza, libre, h_entrada);
							
							//Rellenamos el Array con la plaza creada
							this.array[i][j]=plazaArray;
												
					//Al finalizar, el array debe tener cargadas las plazas
					
				} //cierre de columnas
					
				
			} //cierre de filas
			
		} catch (SQLException e) {
			
			JOptionPane.showMessageDialog(null, "ERROR al cargar Array");

		}

	}  //Cierre del constructor
	
	/**
     * Para saber si la plaza esta libre o no.
     * 
     * @param fila Significa el piso del parking.
     * @param columna Significa la plaza del parking hasta la plaza 19.
     * @return libre Devuelve true si est� la plaza libre, sino false del array bidimensional.
     *  
     */
	public boolean libre(int fila,int columna){
		
		return this.array[fila][columna].isLibre();
	}  //Cierre del m�todo libre
	
	/**
     * Muestra el codigo de cada plaza.
     * 
     * @param fila Significa el piso del parking.
     * @param columna Significa la plaza del parking hasta la plaza 19.
     * @return codigo Es un codigo de la plaza de tipo String del array bidimensional.
     *  
     */
	public String mostrarCodigo(int fila,int columna) {
		
		return this.array[fila][columna].getCodigo();
	} //Cierre del m�todo mostrarCodigo
	
	/**
     * Aparca en el codigo de la plaza seleccionada mediante el piso y el numero de la plaza.
     * 
     * @param piso Significa el piso del parking.
     * @param plaza Significa la plaza del parking hasta la plaza 19.
     * @return codigo Es un codigo de la plaza de tipo String en la que se aparca.
     *  
     */
	public String aparcar (int piso,int plaza){
		
		String codigo="";
		
		for(int i=0;i<this.array[piso].length;i++){ //recorre el piso que le pasamos

			
			if (this.array[piso][plaza].isLibre()==true){ //si la plaza est� libre
				this.libre=false; //Establece el atributo de la clase GestorParking libre como ocupada
				this.array[piso][plaza].setLibre(false); //Establece el atributo libre de la clase Plaza como ocupada
				this.array[piso][plaza].setHora_entrada(this.obtenerHoraSistema()); //Se obtiene la hora del sistema y se establece al atributo h_entrada de la clase Plaza mediante el setter.
				
				codigo=this.toCodigo(piso, plaza); //Se convierte el piso y la plaza al codigo de tipo String

				break; //Sale del bucle
			}
		}
		return codigo;
	} //Cierre del m�todo aparcar
	
	/**
     * Sale el veh�culo del codigo de la plaza seleccionada mediante el piso y el numero de la plaza.
     * 
     * @param piso Significa el piso del parking.
     * @param plaza Significa la plaza del parking hasta la plaza 19.
     *  
     */
	public void salir (int piso, int plaza){
		
		if(this.array[piso][plaza].isLibre()==true){ //Si la plaza est� libre
			JOptionPane.showMessageDialog(null, "La plaza esta vacia");
			
		}
		else{
			this.array[piso][plaza].setLibre(true); //Establece el atributo libre de la clase Vehiculo desde el setter como libre
		}
				
	}  //Cierre del m�todo salir
	

	/**
     * El parking est� lleno si todas las plazas estan llenas.
     * 
     * @return libre Devuelve true si todas las plazas est�n llenas. 
     */
	public boolean estaLLeno(){
		
		for(int i=0;i<filas;i++){
			for(int j=0;j<columnas;j++){
				
				if (this.array[i][j].isLibre()==true){  //si hay plazas libres de la clase Plaza (true)
					this.libre=false; //si la plaza esta vacia, false
					break;
				} //columnas
				else{
					this.libre=true; //si la plaza esta llena, true
				}
				
			}
			
			if (this.libre==false){
				break;
			} //filas
			
		}
		
		return this.libre;  //un retorno que por defecto es false
	}  //Cierre del m�todo estaLLeno
	
	/**
     * El parking est� vacio cuando todas las plazas esten vacias.
     * 
     * @return libre Devuelve false si todas las plazas est�n vacias. 
     * @throws SQLException Contiene informaci�n sobre un error de acceso a la base de datos.
     */
	public boolean estaVacio() throws SQLException{
		
		boolean libre=false;

		String cadena = "SELECT * FROM plazas;";
		ResultSet rs = GestorBD.consulta(stmt, cadena);
		
		while(rs.next()){
			
			libre=rs.getBoolean("libre"); //false es que esta libre

			if(libre==false){ //si hay alguna plaza libre sale del bucle
				break;
			}
			else{
				libre=true;
			}

		}
		return libre;
	} //Cierre del m�todo estaVacio
	
	/**
     * Comprueba si la plaza como par�metro est� ocupada.
     * 
     * @param codigo Es un String que contiene tres n�meros.
     * @return libre Devuelve false si la plaza esta ocupada. 
     * @throws SQLException Contiene informaci�n sobre un error de acceso a la base de datos.
     */
	public boolean plazaOcupada(String codigo) throws SQLException{

		String cadena = "SELECT * FROM plazas WHERE codigo='"+codigo+"';"; //Sentencia de sql guardado en la variable llamado "cadena".
		ResultSet rs = GestorBD.consulta(stmt, cadena); //Se guarda el resultado obtenido de la base de datos
		
		//Mientras que en el resultado haya alg�n dato 
		while(rs.next()){
				
			libre=rs.getBoolean("libre"); //false es que esta ocupada

		}

		return libre;
	} //Cierre del m�todo plazaOcupada
	
	/**
     * Convierte el piso y la plaza en el codigo correspondiente de la plaza de tipo String.
     * 
     * @param piso Es un entero de un s�lo digito.
     * @param plaza Es un entero de dos d�gitos
     * @return codigo Devuelve un String de tres n�meros que contiene el piso y la plaza.
     * 
     */
	public String toCodigo(int piso, int plaza){
		
		String codigo;
		
		if(plaza<=9){
			
			codigo=String.valueOf(piso)+"0"+String.valueOf(plaza);
			
		}else{
			
			codigo=String.valueOf(piso)+String.valueOf(plaza);
		}
		
		return codigo;
				
	} //Cierre del m�todo toCodigo
	
	/**
     * Convierte el codigo de tipo String en un piso de tipo entero.
     * 
     * @param codigo Es un String que contiene tres n�meros.
     * @return piso Devuelve un entero de un d�gito.
     * 
     */
	public int toPiso(String codigo){
		
		String pisostr="";
		pisostr=codigo.substring(0,1);
		
		int piso = Integer.valueOf(pisostr);
		
		return piso;
		
	} //Cierre del m�todo toPiso
	
	/**
     * Convierte el codigo de tipo String en una plaza de tipo entero de dos d�gitos.
     * 
     * @param codigo Es un String que contiene tres n�meros.
     * @return codigo Devuelve un entero de dos d�gitos.
     * 
     */
	public int toPlaza(String codigo){
		
		String plazastr="";
		plazastr = codigo.substring(1);
		
		int plaza = Integer.valueOf(plazastr);
		
		return plaza;
		
	} //Cierre del m�todo toPlaza

	/**
     * Obtiene la hora de sistema 
     * 
     * @return ts Devuelve un Timestamp. Ejemplo: 23/04/2014  13:41:04
     * 
     */
	public Timestamp obtenerHoraSistema(){
		
		//creamos el objeto calendar y obtenemos la hora del sistema
		Calendar cal = Calendar.getInstance();
		
		//Se convierte el objeto calendar a timestamp
		ts = new Timestamp(cal.getTimeInMillis()); 
		
		return ts;
		
	} //Cierre del m�todo obtenerHoraSistema
	
	/**
     * Obtiene la hora de entrada de un plaza en concreta.
     * 
     * @param codigo Es un String que contiene tres n�meros.
     * @return tiempo Devuelve un String la hora de la entrada en una plaza concreta.
     * @throws SQLException Contiene informaci�n sobre un error de acceso a la base de datos.
     * 
     */
	public String tiempo(String codigo)throws SQLException{
		
		String tiempo="";
		
		Timestamp h_entrada=null;

		String cadena = "SELECT * FROM plazas WHERE codigo='"+codigo+"';";
		ResultSet rs = GestorBD.consulta(stmt, cadena);
		
		while(rs.next()){
				
			h_entrada=rs.getTimestamp("h_entrada");
		}
		
		tiempo=this.tiempo(h_entrada);
		
		return tiempo;
	} //Cierre del m�todo tiempo
	
	/**
     * Calcula el tiempo entre la hora de entrada y la hora de salida
     * 
     * @param h_entrada Es un Timestamp de la hora de entrada.
     * @return tiempo Devuelve un String del tiempo transcurrido.
     * 
     */
	@SuppressWarnings("deprecation")
	private String tiempo(Timestamp h_entrada){
		
		String tiempo="";
		String min="";
		String h="";
		String seg="";
		
		Timestamp h_salida=this.obtenerHoraSistema();
		
		long hora=0;
		long minutos=0;
		long segundos=0;
		
		hora=Math.abs(h_salida.getHours()-h_entrada.getHours());
		minutos=Math.abs(h_salida.getMinutes()-h_entrada.getMinutes());
		segundos=Math.abs(h_salida.getSeconds()-h_entrada.getSeconds());
		
			if(hora<=9){
				
				h="0"+String.valueOf(hora);
				
			}else{
				
				h=String.valueOf(hora);
			}
			
			if(minutos<=9){
				
				min="0"+String.valueOf(minutos);
				
			}else{
				
				min=String.valueOf(minutos);
			}
			
			if(segundos<=9){
				
				seg="0"+String.valueOf(segundos);
				
			}else{
				
				seg=String.valueOf(segundos);
			}
	
			tiempo=h+":"+min+":"+seg;

		return tiempo;
	} //Cierre del m�todo tiempo
	
	/**
     * Calcula el coste dependiendo del tiempo obtenido
     * 
     * @param codigo Es un String que contiene tres n�meros.
     * @param h_salida Es un Timestamp de la hora de entrada.
     * @return money Devuelve un String el coste dependiendo del tiempo transcurrido en la plaza concreta.
     * @throws SQLException Contiene informaci�n sobre un error de acceso a la base de datos.
     * 
     */
	public String coste(String codigo,Timestamp h_salida) throws SQLException{
		
		Timestamp h_entrada=null;

		String cadena = "SELECT * FROM plazas WHERE codigo='"+codigo+"';";
		ResultSet rs = GestorBD.consulta(stmt, cadena);
		
		while(rs.next()){
				
			h_entrada=rs.getTimestamp("h_entrada");
		}
				
		String t=this.tiempo(h_entrada);
		
		int pos1=t.indexOf(":");
        String horas=t.substring(0,pos1);
        int pos2=t.indexOf(":", pos1+1);
        String minutos=t.substring(pos1+1, pos2);

        String tiempoS=horas+"."+minutos;
        double tiempo=Double.parseDouble(tiempoS);

		this.coste=2*tiempo;
		DecimalFormat formato = new DecimalFormat("0.00");
		String money=formato.format(this.coste);
		
		return money;
	} //Cierre del m�todo coste
	
	/**
     * Crea un archivo log con los datos del vehiculo y de la plaza.
     * 
     * @param matricula Es un String de la matricula del vehiculo.
     * @param tipo Es un String del tipo del vehiculo.
     * @param marca Es un String de la marca del vehiculo.
     * @param modelo Es un String del modelo del vehiculo.
     * @param color Es un String del color del vehiculo.
     * @param codigo Es un String que contiene tres n�meros.
     * @param evento Es un String para saber si es una entrada o una salida el evento.
     * 
     */
	public void archivoLog(String matricula, String tipo, String marca, String modelo, String color, String codigo, String evento){
		
		try {
			
			FileWriter ficheroW=new FileWriter("src/archivos/archivo.log",true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora=obtenerHoraSistema();
			
			String datos=hora +" "+ codigo +" "+ evento+ "     "+matricula+ "   "+tipo+"  "+marca+"  "+modelo+"  "+ color;

			bW.write(datos);
			bW.newLine();
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	} //Cierre del m�todo archivoLog
	
} //Cierre de la clase
