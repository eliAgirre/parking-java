package controlador;

import vista.VentanaPrincipal;

/**
 * Tiene un atributo para controlar y abrir la VentanaPrincipal.
 * 
 * @author SP,EA
 * @see	VentanaPrincipal
 * @see Main
 */

public class ControladorVentanaPrincipal {

	//Atributos de la clase
	private static VentanaPrincipal ventanaPrincipal;

	/**
     * Constructor sin parámetros.
     */
	public ControladorVentanaPrincipal() {
			
		ventanaPrincipal=new VentanaPrincipal(); //Instancia la VentanaPrincipal
		ventanaPrincipal.setLocationRelativeTo(null); //Coloca la VentanaPrincipal en el centro de la pantalla.
		ventanaPrincipal.setVisible(true); //La VentanaPrincipal es visible
		
				
	} //Cierre del constructor
} //Cierre de la clase