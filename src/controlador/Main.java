package controlador;

import java.io.*;
import java.util.*;
import java.util.Date;

import javax.swing.*;

/**
 * Ejecuta la aplicaci�n y controla si el parking debe estar cerrado.
 * 
 * @author SP,EA
 * @see ControladorVentanaPrincipal
 */

public class Main {
	
	/** 
	 * Ejecuta la aplicaci�n.
	 * 
	 * @param args Array de Strings.
	 * @throws IOException Significa que se ha producido un error en la entrada o en la salida.
	 * @throws InterruptedException 
	 * 
	 */
	@SuppressWarnings({ "deprecation", "unused" })
	public static void main(String[] args) throws IOException, InterruptedException {
		
		//Declaraci�n del atributo ventanaPrincipal de la clase ControladorVentanaPrincipal
		ControladorVentanaPrincipal ventanaPrincipal;

		Date abrir, cerrar; //Declaraci�n de las dos variables llamadas abrir y cerrar de tipo Date.

		abrir= new Date(114,3,25,06,0,0); //establece la hora de abrir del parking (6:00 am)

		cerrar= new Date(114,3,25,00,00,00); //establece la hora del cierre del parking (0:00 am)
        
		//Creaci�n del objeto calendar obteniendo el tiempo del sistema
        Calendar calendario = Calendar.getInstance();

  		int hora, minutos, segundos; //Declaraci�n de las variables llamadas hora,minutos y segundos de tipo entero.
  		
  		hora=calendario.get(Calendar.HOUR_OF_DAY); //Obtiene la hora del sistema
  		minutos=calendario.get(Calendar.MINUTE); //Obtiene los minutos del sistema
  		segundos=calendario.get(Calendar.SECOND); //Obtiene los segundos del sistema
  		
  		Date ahora=new Date(114,3,25,hora,minutos,segundos); //establece la hora del sistema
  		
  		
  		if(ahora.before(abrir)||ahora.after(cerrar)==false){  //La hora del sistema es antes de las 6 y despues de las 12, el Parking est� cerrado.
  			
  			//Se guarda la imagen del parking cerrado como ImageIcon, para despu�s mostrarlo en un aviso por pantalla.
        	ImageIcon cerrado = new ImageIcon("src/vista/Img/cerrado.png");
        	//ImageIcon cerrado = new ImageIcon("C:/Users/agirre/workspace/ProyectoEliSheila/src/vista/Img/cerrado.png");
        	
        	//Muestra el aviso por pantalla "Parking Cerrado".
        	JOptionPane.showMessageDialog(null,cerrado,"Cerrado",JOptionPane.WARNING_MESSAGE);
        	
        	System.exit(0); //Sale de la aplicaci�n
  		}
  		else{ //Si la hora del sistema es a partir de las 6:00 de la madrugada y antes de las 12:00 de la noche, se abre VentanaPrincipal

  			ventanaPrincipal = new ControladorVentanaPrincipal(); //Se instancia la clase ControladorVentanaPrincipal
  		}

	} //Cierre del m�todo main

} //Cierre de la clase 