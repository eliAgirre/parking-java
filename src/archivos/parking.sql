CREATE DATABASE  IF NOT EXISTS `parking` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `parking`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: parking
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `codigo` varchar(7) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` varchar(15) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `tipo_idx` (`tipo`),
  CONSTRAINT `tipo` FOREIGN KEY (`tipo`) REFERENCES `tipo` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES ('AUDI','Audi','Coche'),('BMW','BMW','Coche'),('BMW_M','BMW','Moto'),('CITR','Citroen','Coche'),('CITR_F','Citroen','Furgoneta'),('FER','Ferrari','Coche'),('FIAT','Fiat','Furgoneta'),('FORD','Ford','Coche'),('HON','Honda','Moto'),('KIA','Kia','Furgoneta'),('MERCE','Mercedes','Furgoneta'),('MITS','Mitsubishi','Todoterreno'),('OPEL','Opel','Furgoneta'),('REN','Renault','Todoterreno'),('SKO','Skoda','Todoterreno'),('SUZU','Suzuki','Moto'),('SUZU_T','Suzuki','Todoterreno'),('VOLKS','Volkswagen','Todoterreno'),('YAM','Yamaha','Moto');
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo`
--

DROP TABLE IF EXISTS `modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo` (
  `modelo` varchar(45) NOT NULL,
  `marca` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`modelo`),
  KEY `marca_idx` (`marca`),
  CONSTRAINT `marca` FOREIGN KEY (`marca`) REFERENCES `marca` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo`
--

LOCK TABLES `modelo` WRITE;
/*!40000 ALTER TABLE `modelo` DISABLE KEYS */;
INSERT INTO `modelo` VALUES ('A1','AUDI'),('A3','AUDI'),('A4','AUDI'),('A5','AUDI'),('A6','AUDI'),('i3','BMW'),('Serie 1','BMW'),('Serie 2','BMW'),('Serie 3','BMW'),('Serie 5','BMW'),('Serie 5 GT','BMW'),('C1 200','BMW_M'),('F 800 R','BMW_M'),('F 800 S','BMW_M'),('G 450 X','BMW_M'),('R 1200 ST','BMW_M'),('C-Zero','CITR'),('C1','CITR'),('C3','CITR'),('C4','CITR'),('C4 Cactus','CITR'),('Berlingo','CITR_F'),('Berlingo First','CITR_F'),('C 15','CITR_F'),('C 25','CITR_F'),('Jumper','CITR_F'),('458 Italia','FER'),('458 Spider','FER'),('California','FER'),('California T','FER'),('F12-Berlinetta','FER'),('LaFerrari','FER'),('Doblo','FIAT'),('Doblo Cargo','FIAT'),('Doblo Furgon','FIAT'),('Ducato','FIAT'),('Fiorino','FIAT'),('B-MAX','FORD'),('Fiesta','FORD'),('Focus','FORD'),('Ka','FORD'),('Mondeo','FORD'),('CB 1300 S','HON'),('CB 300 R','HON'),('CBF 600','HON'),('CBR 1000 RR','HON'),('Passion 125 i','HON'),('Besta','KIA'),('K 2500','KIA'),('Pregio','KIA'),('100 D','MERCE'),('140 D','MERCE'),('180 D','MERCE'),('Sprinter','MERCE'),('Transporter','MERCE'),('ASX','MITS'),('L200','MITS'),('Montero','MITS'),('Outlander','MITS'),('Astra Van','OPEL'),('Combo','OPEL'),('Corsa Van','OPEL'),('Kadett Van','OPEL'),('Movano','OPEL'),('Koleos','REN'),('Yeti','SKO'),('Bandit 650','SUZU'),('Burgman 200','SUZU'),('Burgman 400 A','SUZU'),('GSX R 1000','SUZU'),('Sixteen 125','SUZU'),('Grand Vitara','SUZU_T'),('SX 4','SUZU_T'),('SX 4S Cross','SUZU_T'),('Amarok','VOLKS'),('Tiguan','VOLKS'),('Tovareg','VOLKS'),('WR 125 R','YAM'),('WR 125 X','YAM'),('X-MAX 250','YAM'),('XJ 600 N','YAM'),('YZF R 125','YAM');
/*!40000 ALTER TABLE `modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plazas`
--

DROP TABLE IF EXISTS `plazas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plazas` (
  `codigo` varchar(3) NOT NULL,
  `piso` int(1) DEFAULT NULL,
  `plaza` int(2) DEFAULT NULL,
  `libre` tinyint(1) DEFAULT NULL,
  `h_entrada` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plazas`
--

LOCK TABLES `plazas` WRITE;
/*!40000 ALTER TABLE `plazas` DISABLE KEYS */;
INSERT INTO `plazas` VALUES ('000',0,0,1,NULL),('001',0,1,1,NULL),('002',0,2,1,NULL),('003',0,3,1,NULL),('004',0,4,1,NULL),('005',0,5,1,NULL),('006',0,6,1,NULL),('007',0,7,1,NULL),('008',0,8,1,NULL),('009',0,9,1,NULL),('010',0,10,1,NULL),('011',0,11,1,NULL),('012',0,12,1,NULL),('013',0,13,1,NULL),('014',0,14,1,NULL),('015',0,15,1,NULL),('016',0,16,1,NULL),('017',0,17,1,NULL),('018',0,18,1,NULL),('019',0,19,1,NULL),('100',1,0,1,NULL),('101',1,1,1,NULL),('102',1,2,1,NULL),('103',1,3,1,NULL),('104',1,4,1,NULL),('105',1,5,1,NULL),('106',1,6,1,NULL),('107',1,7,1,NULL),('108',1,8,1,NULL),('109',1,9,1,NULL),('110',1,10,1,NULL),('111',1,11,1,NULL),('112',1,12,1,NULL),('113',1,13,1,NULL),('114',1,14,1,NULL),('115',1,15,1,NULL),('116',1,16,1,NULL),('117',1,17,1,NULL),('118',1,18,1,NULL),('119',1,19,1,NULL),('200',2,0,1,NULL),('201',2,1,1,NULL),('202',2,2,1,NULL),('203',2,3,1,NULL),('204',2,4,1,NULL),('205',2,5,1,NULL),('206',2,6,1,NULL),('207',2,7,1,NULL),('208',2,8,1,NULL),('209',2,9,1,NULL),('210',2,10,1,NULL),('211',2,11,1,NULL),('212',2,12,1,NULL),('213',2,13,1,NULL),('214',2,14,1,NULL),('215',2,15,1,NULL),('216',2,16,1,NULL),('217',2,17,1,NULL),('218',2,18,1,NULL),('219',2,19,1,NULL);
/*!40000 ALTER TABLE `plazas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo`
--

DROP TABLE IF EXISTS `tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo` (
  `codigo` varchar(15) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo`
--

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
INSERT INTO `tipo` VALUES ('Coche'),('Furgoneta'),('Moto'),('Todoterreno');
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiculo` (
  `matricula` varchar(7) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `marca` varchar(15) DEFAULT NULL,
  `modelo` varchar(15) DEFAULT NULL,
  `color` varchar(15) DEFAULT NULL,
  `codigo` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`matricula`),
  KEY `codigo_idx` (`codigo`),
  CONSTRAINT `codigo` FOREIGN KEY (`codigo`) REFERENCES `plazas` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-15 16:03:31
