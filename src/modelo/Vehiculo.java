package modelo;

/**
 * Tiene varios atributos propios para que pueda guardar los datos de la base de datos en la tabla Vehiculo y un atributo que hace referencia a la tabla Plazas.
 * 
 * @author SP,EA
 */

public class Vehiculo {

	//Atributo de la clase
	private String matricula; //clave principal de la tabla Vehiculo
	private String tipo;
	private String marca;
	private String modelo;
	private String color;
	private String codigo; //clave extranjera de la tabla Plazas.
	
	/**

     * Constructor como par�metros todos los atributos.

     * @param matricula Define la matricula de un Vehiculo sea coche, una moto,etc.
     * @param tipo Define el tipo de un Vehiculo.
     * @param marca Define la marca de un Vehiculo.
     * @param modelo Define el modelo del Vehiculo que depender� de la marca.
     * @param color Define el color del Vehiculo.
     * @param codigo Define el codigo, consiste en el piso y el n�mero de la plaza que estar� aparcado.

     */
	public Vehiculo(String matricula, String tipo, String marca, String modelo, String color, String codigo) {
	
		this.matricula = matricula;
		this.tipo = tipo;
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;
		this.codigo = codigo;
		
	} //Cierre del constructor
	
	/**
     * Obtener la matricula de la clase Vehiculo desde otra clase.
     * 
     * @return matricula Devuelve la matricula de un Vehiculo.
     */
	public String getMatricula() {
		return matricula;
	}
	
	/**
     * Acceder al tipo de la clase Vehiculo desde otra clase.
     * 
     * @return tipo Devuelve el tipo del Vehiculo, ya sea un coche, un cami�n, una moto,etc.
     */
	public String getTipo() {
		return tipo;
	}

	/**
     * Obtener la marca desde otra clase del mismo paquete o de otro paquete.
     * 
     * @return marca Devuelve la marca de un Vehiculo.
     */
	public String getMarca() {
		return marca;
	}

	/**
     * Acceder al modelo de la clase Vehiculo desde otra clase del mismo paquete o de otro.
     * 
     * @return modelo Devuelve el modelo de un Vehiculo.
     */
	public String getModelo() {
		return modelo;
	}

	/**
     * Obtener el color desde otra clase.
     * 
     * @return color Devuelve el color de un Vehiculo.
     */
	public String getColor() {
		return color;
	}

	/**
     * Acceder al atributo codigo del m�todo getter desde otra clase del mismo paquete o de otro paquete.
     * 
     * @return codigo Devuelve codigo en un String de 3 caracteres.
     */
	public String getCodigo() {
		return codigo;
	}
	
	//Cierre de m�todos getters.
	
} //Cierre de la clase 