package modelo;

/**
 * Tiene un atributo para que pueda guardar los datos de la base de datos de la tabla Tipo al crear el objeto de esta clase y mostrar el tipo en el primer JComboBox en la clase VentanaEntrar.
 * 
 * @author SP,EA
 */

public class Tipo {

	//Atributos de la clase
	private String codigo; //tipo de Veh�culo (coche,moto,furgoneta,todoterreno)

	
	/**
     * Constructor tiene como par�metro un �nico atributo
     * @param codigo Define el tipo de Vehiculo. (coche,moto,furgoneta,todoterreno)
     */
	public Tipo(String codigo) {
		
		this.codigo = codigo;
	} //Cierre del constructor

	/**
     * Para que puedan acceder desde otra clase del mismo paquete u otro paquete.
     * @return tipo Devuelve el tipo de un Vehiculo.
     */
	public String getCodigo() {
		return codigo;
	}
	//Cierre de m�todos getters.
	
	/**
     * Para que en el JComboBox aparezca el codigo en la llamada "VentanaEntrar".
     * 
     * @return El nombre. Devuelve el codigo de Tipo
     */
	public String toString() {
		return this.codigo;
	} //Cierre del m�todo toString
} //Cierre de la clase 