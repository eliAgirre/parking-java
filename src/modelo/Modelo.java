package modelo;

/**
 * Tiene dos atributos para que pueda guardar los datos de la base de datos de la tabla Modelo al instanciarlo y mostrar el modelo en el JComboBox en la clase VentanaEntrar.
 * 
 * @author SP,EA
 */

public class Modelo {

	//Atributos de la clase
	private String modelo; //clave principal de la tabla Modelo
	private String marca; //clave extranjera de la tabla Marca
	
	/**
      * Constructor con par�metros todos los atributos.
     * 
     * @param modelo Define un �nico Modelo dentro de la Marca de un veh�culo.
     * @param marca Es la marca de un coche.

     */
	public Modelo(String modelo, String marca) {

		this.modelo = modelo;
		this.marca = marca;
	} //Cierre del constructor

	/**

     * Para obtener el modelo desde otra clase.
     * @return modelo Devuelve el modelo de una Marca.
     */
	public String getModelo() {
		return modelo;
	}

	/**

     * Para obtener la marca desde otra clase del mismo paquete u de otro.
     * @return marca Devuelve la marca.
     */
	public String getMarca() {
		return marca;
	}

	//Cierre de m�todos getters.
	
	/**
     * Para que en el JComboBox aparezca el modelo en la ventanas llamada "VentanaEntrar".
     * 
     * @return modelo Devuelve el modelo.
     */
	public String toString(){  //Se sobreescribe el m�todo toString que se hereda de la superclase Object.
		
		return this.modelo;
	} //Cierre del m�todo toString
	
} //Cierre de la clase 