package modelo;

import java.sql.*;

/**
 * Contiene varios atributos para que pueda guardar los datos en la base de datos en la tabla Plaza.
 * 
 * @author SP,EA
 */

public class Plaza {
	
	//Atributos de la clase
	private String codigo; //clave principal de la tabla Plazas
	private int piso;
	private int plaza;
	private boolean libre;
	private Timestamp h_entrada;
	
	/**

     * Constructor como par�metros todos de los atributos.

     * @param codigo Define un �nico codigo para cada plaza del Parking.
     * @param piso Define el piso que constituye en el primer d�gito del codigo.
     * @param plaza Define una plaza que contiene dos d�gitos porque en cada piso hay 20 plazas.
     * @param libre Define si la plaza est� libre o est� llena.
     * @param h_entrada Define a la hora que ha entrado al parking un vehiculo dependiendo del c�digo de la plaza.

     */
	public Plaza(String codigo, int piso, int plaza, boolean libre, Timestamp h_entrada) {
	
		this.codigo = codigo;
		this.piso = piso;
		this.plaza = plaza;
		this.libre = libre;
		this.h_entrada = h_entrada;

	} //Cierre del constructor
	
	/**
     * Obtener el codigo de la clase Plaza desde otra clase del mismo paquete o de otro.
     * 
     * @return codigo Devuelve el codigo de una Plaza de tipo String.
     */
	public String getCodigo() {
		return codigo;
	}

	/**
     * Acceder al piso de la clase Plaza desde otra clase.
     * 
     * @return piso Devuelve la piso de una Plaza en enteros.
     */
	public int getPiso() {
		return piso;
	}

	/**
     * Obtener el numero de la plaza de la clase Plaza.
     * 
     * @return plaza Devuelve la parcela de una Plaza en enteros.
     */
	public int getPlaza() {
		return plaza;
	}

	/**
     * Acceder al atributo libre del m�todo getter desde otra clase del mismo paquete o de otro paquete.
     * 
     * @return libre Devuelve si una Plaza est� libre (true) o vacia (false).
     */
	public boolean isLibre() {
		return libre;
	}

	/**
     * Obtener la hora de entrada de la plaza.
     * 
     * @return h_entrada Devuelve la hora de entrada de una Plaza.
     */
	public Timestamp getH_entrada() {
		return h_entrada;
	}

	//Cierre de m�todos getters.
	
	/**
     * Para establecer el atributo libre desde otra clase.
     * 
     * @param libre Establece una Plaza si esta lleno (false) cuando un vehiculo aparca, y cuando sale el atributo libre es true.
     */
	
	public void setLibre(boolean libre) {
		this.libre = libre;
	}


	/**
     * Para modificar el atributo hora de entrada.
     * 
     * @param h_entrada Establece la hora de entrada.
     */
	public void setHora_entrada(Timestamp h_entrada) {
		this.h_entrada = h_entrada;
	} 
	//Cierre de los setters
	
} //Cierre de la clase 