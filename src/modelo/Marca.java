package modelo;

/**
 * 
 * Tiene dos atributos para que pueda guardar los datos de la base de datos de la tabla Marca cuando se crea el objeto de tipo Marca y mostrar el nombre en el JComboBox en la clase VentanaEntrar.
 * 
 * @author SP,EA
 * @see	Modelo
 */

public class Marca {

	//Atributos de la clase
	private String codigo; //clave principal en la tabla Marca.
	private String nombre; //nombre de la Marca
	
	/**
     * Constructor con par�metros todos los atributos.
     * 
     * @param codigo Define un nombre corto o clave de una Marca.
     * @param nombre Define el nombre completo de la Marca.
     */
	public Marca(String codigo, String nombre) {

		this.codigo = codigo;
		this.nombre = nombre;
		
	} //Cierre del constructor

	/**

     * Para obtener el codigo desde otra clase.
     * @return codigo Devuelve el codigo de un Marca.
     */
	public String getCodigo() {
		return codigo;
	}
	
	/**
     * Para obtener el nombre desde otra clase del mismo paquete u de otro.
     * @return nombre Devuelve el nombre de una Marca.
     */
	public String getNombre() {
		return nombre;
	}
	
	//Cierre de m�todos getters.
	
	/**
      * Para que en el JComboBox aparezca el nombre de la Marca en la ventana llamada "VentanaEntrar". 
      * 
      * @return nombre Devuelve el nombre de un Marca.
      */
	public String toString() {
		return this.nombre;
	} //Cierre del m�todo toString

} //Cierre de la clase 