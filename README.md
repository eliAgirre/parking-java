# parking #

Se trata de una aplicación de Java para un parking. Al introducir los datos del vehículo podrá elegir la plaza, se le dará un código para que después pueda salir y pagar el servicio de parking.

### Estructura de la app ###

| Nombre                             | Descripción                                                 |
| ---------------------------------- |:-----------------------------------------------------------:|
| **archivos**/                      | Guarda los logs en los archivos externos.                   |
| **controlador**/                   | Intermediario entre el modelo y la vista.                   |
| **gestor**/                        | Son clases que contienen ArrayList o conexión a la BD.      |
| **modelo**/                        | Es la parte del servidor para obtener y manejar datos.      |
| **vista**/                         | El cliente visualiza la app.                                |
| main.java                          | Ejecuta la aplicación.                                      |




### Funcionamiento de la app: ###

    MySQL → Base de datos relacional en localhost.
    Controlador→ Controlar los datos de la base de datos y los que introduce el usuario.
    Vista → Visualizar datos para el usuario.

### Tecnologías a usar: ###

    - Java POO con el IDE Eclipse
    - MySQL → Base de datos relacional.

### Sistema operativo: ###

    - Windows 7